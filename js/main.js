const BASE_URL = "https://63f85e4e5b0e4a127de45517.mockapi.io";

function fetchDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      var dsSV = res.data;
      renderDSSV(dsSV);
      tatLoading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatLoading();
    });
}
function renderDSSV(dsSV) {
  var contentHTML = "";
  for (var i = dsSV.length - 1; i >= 0; i--) {
    var item = dsSV[i];
    var contentTr = `
      <tr>
          <td> ${item.ma} </td>
          <td> ${item.ten} </td>
          <td> ${item.email} </td>
          <td> 0 </td>
          <td>
          <button onclick="xoaSV('${item.ma}')" class="btn btn-danger">
          Xóa
          </button>
          <button onclick="suaSV('${item.ma}')" class="btn btn-danger">
          Sửa
          </button>
          </td>
      </tr>
       `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
fetchDSSV();
function xoaSV(id) {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDSSV();
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function suaSV(id) {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function capNhatSV() {
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      fetchDSSV();
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function themSV() {
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      fetchDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
